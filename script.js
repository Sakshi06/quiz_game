//Variables
let playing = false;
let score;
let timeremaining;
let countdown;
let correctAns;

//Required Elements
let startReset =document.getElementById("startreset");

//Event Registrations
startReset.addEventListener('click',startResetGame);
document.getElementById("box1").addEventListener('click',processAnswer);
document.getElementById("box2").addEventListener('click',processAnswer);
document.getElementById("box3").addEventListener('click',processAnswer);
document.getElementById("box4").addEventListener('click',processAnswer);

//Helper functions
function setText(id, text){
    document.getElementById(id).innerHTML = text;
    
}
function show(id){
    document.getElementById(id).style.display='block';
}

function hide(id){
    document.getElementById(id).style.display ='none';
}
function startResetGame(e){
    if(playing === true){
        //game is on and you want to reset
        setText("startreset", "Start Game");
        show("gameover");
        
        setText("gameover","<p>Game Over!</p><p> Your Score : "+score +"</p>");
        setText("scoreValue", "");
        hide("timeremaining");
    }else{
        //game is off and you want to start a new game
        setText("startreset", "Reset Game");
        
        score = 0;
        setText("scoreValue", score);
        
        show("timeremaining");
        timeremaining = 60;
        setText("timeremainingvalue",timeremaining);
        
        hide("gameover");
        
        startCountdown();
        generateQA();
    }
    playing = !playing;
}
function startCountdown(){
    countdown = setInterval(function(){
        timeremaining -= 1;
        setText("timeremainingvalue", timeremaining);
        if(timeremaining <= 0){
            clearInterval(countdown);
            show("gameover");
            setText("gameover","<p>Game Over!</p><p>Your Score : "+ score +"</p>");
            setText("scoreValue", "");
            hide("timeremaining");
            
            //as timer out , game is over so we not playing
            playing =false;
            setText("startreset", "Start Game");
        }
    }, 1000);
}
function generateQA(){
    let no1 = Math.round((1 + Math.random() * 9));
    let no2 = Math.round((1 + Math.random() * 9));
    
    correctAns = no1 * no2;
    
    setText("question", no1 +" x "+ no2);
    
    let correctPosition = Math.round((1 + Math.random() * 3));
    setText("box"+correctPosition, correctAns);
    
    let answers =[correctAns];
    for(let i = 1; i<5; i++){
        
        let wrongAnswer;
        if(i != correctPosition){
        
            do{
                wrongAnswer = Math.round(1 + Math.random() * 9) * Math.round((1 + Math.random() * 9))
            }while(answers.indexOf(wrongAnswer) > -1);
        
            setText("box"+i, wrongAnswer);
            answers.push(wrongAnswer);
        }
    
    }
}
function processAnswer(){
    if(playing === true){
        if(correctAns == this.textContent){
            score++;
            setText("scoreValue", score);
            show("correct");
            hide("wrong");
            setTimeout(function(){
                hide("correct");
            },1000);
            generateQA();
        }else{
            show("wrong");
            hide("coreect");
            setTimeout(function(){
                hide("wrong");
                
            }, 1000);
        }
    }

}